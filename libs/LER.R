#### FUNKTIONEN FÜR DEN LER

# Gaußsche Fehlerfortpflanzung des LER
errorLER <- function(inter_yield_1, sole_yield_1, inter_yield_2, sole_yield_2){
  summand_1 <- (sd(sole_yield_1)/mean(inter_yield_1))^2
  summand_2 <- (mean(sole_yield_1)*sd(inter_yield_1)/(mean(inter_yield_1)^2))^2
  summand_3 <- (sd(sole_yield_2)/mean(inter_yield_2))^2
  summand_4 <- (mean(sole_yield_2)*sd(inter_yield_2)/(mean(inter_yield_2)^2))^2
  
  error_LER_val <- sqrt(summand_1+summand_2+summand_3+summand_4)
  
  return(error_LER_val)
}

# Berechnung des LER für ein speuifisches Treatment, an einem bestimmten Tag
LER_detailed <- function(data, code.crop, cultivarSW , fert, variable, expand=F, meanRep=F, solelyMean=F, mixMean=F){
  if(fert==100){
    fertFB <- 50
  }
  else{
    fertFB <- fert
  }
  
  ySW_mix <- subset(data, data$code_crop==code.crop & data$cultivar==cultivarSW)[c(variable, "date_count", "obs_date", "code_crop", "cultivar", "cropping_system", "treatment", "Plot_ID", "replicates")]
  
  ySW_sole <- subset(data, data$cultivar==cultivarSW & data$treatment==fert & data$cropping_system=="solely")[variable]
  
  yFB_mix <- subset(data, data$code_crop==code.crop & data$cultivar=="Fanfare")[variable]
  
  yFB_sole <- subset(data, data$cultivar=="Fanfare" & data$treatment==fertFB & data$cropping_system=="solely")[variable]
  
  metaDf <- ySW_mix[c("date_count", "obs_date", "code_crop", "cultivar", "cropping_system", "treatment", "Plot_ID", "replicates")]
  ySW_mix <- ySW_mix[variable]
  
  if(solelyMean){
    ySW_sole <- mean(unlist(ySW_sole))
    yFB_sole <- mean(unlist(yFB_sole))
  }
  
  if(mixMean){
    ySW_mix <- mean(unlist(ySW_mix))
    yFB_mix <- mean(unlist(yFB_mix))
  }
  
  if(expand & !solelyMean & !mixMean){
    grid <- expand.grid(1:4, 1:4, 1:4, 1:4)
    ySW_mix <- ySW_mix[[variable]][grid[,1]]
    ySW_sole <- ySW_sole[[variable]][grid[,2]]
    yFB_mix <- yFB_mix[[variable]][grid[,3]]
    yFB_sole <- yFB_sole[[variable]][grid[,4]]
  }
  
  plerSW <- unlist(ySW_mix)/unlist(ySW_sole) 
  plerFB <- unlist(yFB_mix)/unlist(yFB_sole) 
  
  LERval <- plerSW + plerFB
  
  returnFrame <- data.frame(obs_date=metaDf$date, date_count=metaDf$date_count, code_crop=metaDf$code_crop, treatment=metaDf$treatment, plerSW, plerFB, LERval)
  rownames(returnFrame) <- NULL
  names(returnFrame) <- c("obs_date", "date_count", "code_crop", "treatment", "PLER_SW", "PLER_FB", "LER")
  
  if(meanRep){
    ySW_mix_mean <- mean(unlist(ySW_mix))
    ySW_sole_mean <- mean(unlist(ySW_sole))
    yFB_mix_mean <- mean(unlist(yFB_mix))
    yFB_sole_mean <- mean(unlist(yFB_sole))
    
    plerSW_mean <- ySW_mix_mean/ySW_sole_mean
    plerFB_mean <- yFB_mix_mean/yFB_sole_mean
    
    LERval_mean <- plerSW_mean + plerFB_mean
    
    plerSW_mean <-  rep(plerSW_mean, nrow(returnFrame))
    plerFB_mean <- rep(plerFB_mean, nrow(returnFrame))
    
    LERval_mean <- rep(LERval_mean, nrow(returnFrame))
    
    returnFrame$PLER_SW_mean <- plerSW_mean
    returnFrame$PLER_FB_mean <- plerFB_mean
    
    returnFrame$LER_mean <- LERval_mean
    
  }
  
  return(returnFrame)
}

# Berechnung des LER über alle Tage und über alle Treatments
LER_wrapper <- function(dataSet, variableName, gridExpand=F, soleMean=F, interMean=F){
  returnFrame <- data.frame(matrix(nrow=0, ncol=10))
  colnames(returnFrame) <- c("obs_date", "date_count", "code_crop", "treatment", "PLER_SW", "PLER_FB", "LER", "PLER_SW_mean", "PLER_FB_mean", "LER_mean")
  
  iterationVec <- unique(dataSet$date_count)
  
  iterationFrame <- data.frame(mix_crop_code = c("Ana_Fan_0", "Ana_Fan_50", "Ana_Fan_100", "SUA_Fan_0", "SUA_Fan_50", "SUA_Fan_100"),
                               SW = c(rep("Anabel",3),rep("SUAhab",3)),
                               treatment = rep(c(0,50,100),2))
  
  for(i in iterationVec){
    dataSetSub <- subset(dataSet, dataSet$date_count==i)
    for(j in 1:nrow(iterationFrame)){
      currentFrame <- LER_detailed(dataSetSub, iterationFrame$mix_crop_code[j], iterationFrame$SW[j], iterationFrame$treatment[j], variableName, meanRep = T, expand = gridExpand, solelyMean = soleMean, mixMean = interMean)
      
      returnFrame <- rbind(returnFrame, currentFrame)
    }
  }
  
  return(returnFrame)
}